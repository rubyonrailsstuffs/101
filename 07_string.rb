x = "Ruby"
y = 'Ruby'

p x.class
p y.class

a = 'Ruby on '
b = 'Rails'

p a << b # Same object
p a + b # New object

# String interpolation

c = 'Railssss'
d = "#{1 + 1} Ruby on #{c}" # only double quotes

p d