# Create empty array
v = Array.new # or v = []

v.push(1)
v.push('Hello')
v.push('Ruby101')
v.push(2.6)

puts v
puts v[2]


# Nested array
arr = [[11,12,13],[21,22,23],[31,32,33]]

arr.each do |arr_main|
  arr_main.each do |arr_item|
    puts arr_item
  end
end