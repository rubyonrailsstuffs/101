#each
(0..5).each do |i|
  puts "Value: " + i.to_s
end

#while
i = 0
num = 5

while i < num do
  puts "Counting... " + i.to_s
  i += 1
end