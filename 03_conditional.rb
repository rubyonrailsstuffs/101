# case
print 'Type a age '
age = gets.chomp.to_i

case age
  when 0..2
    puts "baby"
  when 3..12
    puts "kid"
  when 13..18
    puts "teenager"
  else
    puts "adult"
end

puts '======'

#unless
print 'Type a number '
x = gets.chomp.to_i

unless x >= 5
  puts 'x < 5'
else
  puts 'x >= 5'
end

puts '======'

#if
print 'Type a number '
x = gets.chomp.to_i

if x > 2
  puts 'x > 2'
end