# Ruby on Rails

#DRY #CoC #TheRailsWay

## Módulo 1

### Referências

- [Ruby on Rails: What it is and why you should use it for your web application](https://bitzesty.com/2014/03/03/ruby-on-rails-what-it-is-and-why-we-use-it-for-web-applications/)

- [Ruby website](https://www.ruby-lang.org/pt/)

- [RubyGems](https://rubygems.org/?locale=pt-BR)

- [Ruby on Rails](https://rubyonrails.org/)

### Configuração do ambiente local

- Pacotes básicos
  ```
  sudo apt-get install -y build-essential autoconf bison libssl-dev libyaml-dev libreadline-dev zlib1g-dev libncurses5-dev libffi-dev libgdbm-dev libpq-dev curl ruby-full
  ```

- RVM
  ```
  gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3

  curl -sSL https://get.rvm.io | bash

  source /home/<user>/.rvm/scripts/rvm
  ```

- Ruby
  ```
  rvm list known

  rvm install 2.7

  rvm list

  rvm use 2.7 --default
  ```

  [Integrating RVM with gnome-terminal](https://rvm.io/integration/gnome-terminal)

- Rails
  ```
  gem install rails -v 5.2
  ```

- Node.js via nvm
  ```
  curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
  ```

### Criando o primeiro projeto

```
rails new <project_name>

cd <project_name>

rails server
```

> Yay! You’re on Rails!

## Módulo 2

### RVM (Ruby Version Manager)

[https://rvm.io](https://rvm.io)

### Comandos úteis

- Listar todas as versões disponíveis no repositório

  `rvm list known`

- Atualizar a lista

  `rvm get head`

- Listar as versões instaladas localmente

  `rvm list`

- Instalar uma versão escolhida

  `rvm install x.x.x`

- Instalar uma versão escolhida e a torna padrão

  `rvm install x.x.x --default`

- Usar uma versão específica do Ruby

  `rvm use x.x.x`

### Pry

`gem install pry`

#### Pry padrão ao chamar o IRB

- Criar o arquivo .irbcrc na home do usuário

  `touch ~/.irbrc`

- Dentro do arquivo .irbc
  ```
  require 'rubygems'
  require 'pry'
  Pry.start
  exit
  ```

---

# Ruby 101

## Variáveis

```
x = 1
y = 2.5
z = 'Ruby 101'
```

### Verificar o tipo da variável

Usar o método `.class`

```
x.class
=> Integer

y.class
=> Float

z.class
=> String
```

## IO

- Saída padrão

  `puts`

- Saída padrão sem quebra de linha

  `print`

- Entrada padrão

  `gets`

- Remover código de formação

  `.chomp`

### Casting

Transformar o conteúdo de uma variável em outro tipo

Exemplos:

- Float

  `to.f`

- Integer

  `to_i`

- String

  `to_s`

## Hashes

Uma lista do tipo chave/valor

- Tradicional

  `h = {"Rails" => 5, "Ruby" => 1.9}`

- ^1.9

  `h = {"Rails": 5, "Ruby": 1.9}`

Para acessar os elementos, usa-se []

  `h["Rails"]`

## Symbols

São strings imutáveis e usados em situações onde precisamos de um identificador pois é garantido que seu uso não implicará na criação de um novo objeto.

## Tipos primitivos vs complexos

- Tipos primitivos

  - Inteiro

    `x = 1`

  - Real/ Float

    `y = 3.45`

  - Strings

    `z = 'abc'`

  - Booleano

    `w = true`

- Tipos complexos

Usamos tipos primitivos para criar os complexos. No ruby eles são classes.

Por exemplo a classe `Date`: 22/02/2020 = int str int str int

## Classes vs Objetos

A classe é a forma e o objeto é o elemento pronto. A classe é a maneira que temos de informas como queremos que nosso objeto funcione. E assim especificamos os métodos (ações) e atributos (características) que cada objeto terá.

Tudo no Ruby é objeto! `.class`!

Lembre-se: uma classe instanciada é um objeto! Podemos instanciar de duas formas:

- Por inferência

`a = [12, 23, 34]`

- Por declaração explícita

`b = Array.new`
`b.push(12)`

### Método *initialize*

É um método especial que serve para indicarmos o que a classe deve fazer ao ser instanciada.

```
def initialize
  p 'Initializing...'
end
```

### Self

É o próprio objeto, ou seja, o objeto que foi instanciado.

```
def my_id
  self.object_id
end
```

### Variáveis de instância

Só existem apenas na instância do objeto, ou seja, cada objeto possui seus próprios valores em tais variáveis.

São precedidas de um `@`.

### Accessors

Servem como atalhos para declaração de atributos de uma classe.

`attr_accessor :name`

Essa declaração te dá um "getter" e um "setter" para `name` na classe em questão.

### Herança

Usa-se o sinal `<` para indicar herança.

No Ruby não existe herança múltipla, não é possível herdar de várias classes ao mesmo tempo.

### Métodos de Instância (MI) vs Métodos de Classe (MC)

MI são os métodos que só podem ser invocados a partir de um objeto, ou seja, uma classe instanciada.

Já MC podem ser executados a partir da própria classe, não sendo necessário uma instância dela.

## Módulos e Mixins

São similares a classes em relação ao fato de que também armazenam uma coleção de **métodos**, **constantes** e outras definições de **módulos** e **classes**.

Não podemos instanciar um módulo, ao invés disso, especificamos funcionalidades que desejamos adicionar a uma classe ou objeto.

Prós:

  - Agem como *namespace*, permitindo definir métodos com nomes já utilizados em outra parte do projeto.

  - Permite compartilhar funcionalidades entre classes - se uma classe "mistura" (*mixes in*) um módulo (o inclui), todos os métodos de instância do módulo se tornam disponíveis naquela classe. Isso resolve o problema de não ter herança multipla no Ruby.

## Gems

Pesquisando... https://rubygems.org

### Listando

- Instaladas

  `gem list`

- Pesquisa aproximada localmente

  `gem list <nome_gem>`

- Pesquisa aproximada remotamente

  `gem list <nome_gem> --remote`

- Pesquisa aproximada remotamente para todas as versões

  `gem list <nome_gem> --remote --all`

### Instalando

- Simples

  `gem install <nome_gem>`

- Versão específica

  `gem install <nome_gem> -v <x.x.x>`

### Removendo

- Apenas uma

  `gem uninstall <nome_gem>`

- Apenas uma versão específica

  `gem uninstall <nome_gem> -v <x.x.x>`

- Versões antigas de todas as gems

  `gem cleanup`

- Versões antigas de um gem específica

  `gem cleanup <nome_gem>`

- Verificar versões que serão apagadas

  `gem cleanup -d`

### Versionamento

X.Y.Z. (Major.Minor.Patch)

- Versão exata

  `gem '<nome_gem>', '1.0.0'`

- Versão igual ou maior

  `gem '<nome_gem>', '>=2.0.0'`

- Versão parcial

  `gem '<nome_gem>', '~>1.6.0'` Sempre será < 2.0.0

## Bundler

Gem responsável por gerenciar as dependências do projeto. https://bundler.io/

## Algumas gems...

- [TTY::Spinner](https://github.com/piotrmurach/tty-spinner)

- [LeroleroGenerator](https://github.com/jacksonpires/lerolero_generator)

# Ruby on Rails

## Docs

[Rails](https://guides.rubyonrails.org/), [APIdock](https://apidock.com/) & [Ruby](https://ruby-doc.org/)

## Estrutura do diretório

`/app`: diretório principal

`/app/controlles`: interceptar as requisições/ IO

`/app/helpers`: lógicas complexas

`/app/models`: modelagem dos dados/ entidades

`/app/views`: front-end

`/bin`: scripts

`/config`: configurações gerais (tradução, ambientes e outros)

`/db`: configurações do banco de dados

`/lib`: módulos externos

`/log`: logs da aplicação

`/public`: arquivos públicos/ usuário final

`/test`: arquivos de testes

`/tmp`: arquivos temporários (cache, sessions e outros)

`/vendor`: aplicações externas/ plugins