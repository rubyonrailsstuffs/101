class User
  attr_accessor :name, :email
end

class Customer < User
  attr_accessor :cpf

  def speak
		'Hello!'
  end
end

class Provider < User
  attr_accessor :cnpj

  def provide
		'Delivering...'
  end
end

#User setter
u = User.new
u.name = 'User 1'
u.email = 'user@email.com'

#User getter
p u.name
p u.email

#Customer setter
c = Customer.new
c.name = 'Customer 1'
c.email = 'customer@email.com'
c.cpf = '12345612312'

#Customer getter
p c.name
p c.email
p c.cpf
p c.speak

#Provider setter
pro = Provider.new
pro.name = 'Provider 1'
pro.email = 'provider@email.com'
pro.cnpj = '123456123120001'

#Provider getter
p pro.name
p pro.email
p pro.cnpj
p pro.provide